package org.jeecgframework.web.activiti.dao;

import org.jeecgframework.minidao.annotation.Param;
import org.jeecgframework.minidao.annotation.Sql;
import org.jeecgframework.web.activiti.entity.OaLeaveEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface LeaveDao {

	@Sql("select * from oa_leave")
	OaLeaveEntity getLeave(@Param("id") Long id);
}
