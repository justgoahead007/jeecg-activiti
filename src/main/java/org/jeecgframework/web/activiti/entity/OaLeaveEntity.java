package org.jeecgframework.web.activiti.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.jeecgframework.poi.excel.annotation.Excel;

@Entity
@Table(name = "oa_leave", schema = "")
@SuppressWarnings("serial")
public class OaLeaveEntity implements java.io.Serializable {
    /**id*/
    private java.lang.Integer id;
    /**reason*/
    @Excel(name="reason",width=15)
    private java.lang.String reason;
    /**userId*/
    @Excel(name="userId",width=15)
    private java.lang.String userId;
    /**processInstanceId*/
    @Excel(name="processInstanceId",width=15)
    private java.lang.String processInstanceId;

    /**
     *方法: 取得java.lang.Integer
     *@return: java.lang.Integer  id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    @Column(name ="ID",nullable=false,length=10)
    public java.lang.Integer getId(){
        return this.id;
    }

    /**
     *方法: 设置java.lang.Integer
     *@param: java.lang.Integer  id
     */
    public void setId(java.lang.Integer id){
        this.id = id;
    }
    /**
     *方法: 取得java.lang.String
     *@return: java.lang.String  reason
     */

    @Column(name ="REASON",nullable=true,length=45)
    public java.lang.String getReason(){
        return this.reason;
    }

    /**
     *方法: 设置java.lang.String
     *@param: java.lang.String  reason
     */
    public void setReason(java.lang.String reason){
        this.reason = reason;
    }
    /**
     *方法: 取得java.lang.String
     *@return: java.lang.String  userId
     */

    @Column(name ="USER_ID",nullable=true,length=45)
    public java.lang.String getUserId(){
        return this.userId;
    }

    /**
     *方法: 设置java.lang.String
     *@param: java.lang.String  userId
     */
    public void setUserId(java.lang.String userId){
        this.userId = userId;
    }
    /**
     *方法: 取得java.lang.String
     *@return: java.lang.String  processInstanceId
     */

    @Column(name ="PROCESS_INSTANCE_ID",nullable=true,length=32)
    public java.lang.String getProcessInstanceId(){
        return this.processInstanceId;
    }

    /**
     *方法: 设置java.lang.String
     *@param: java.lang.String  processInstanceId
     */
    public void setProcessInstanceId(java.lang.String processInstanceId){
        this.processInstanceId = processInstanceId;
    }
}
